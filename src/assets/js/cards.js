$( document ).ready(function() {
  var touch = {
    end: null,
    move: null,
    start: null
  };
  var current;
  if(document.getElementsByClassName('boxCard')[0]){
    var box = document.getElementsByClassName('boxCard')[0];
    box.addEventListener('swipeleft', handleSwipe);
    box.addEventListener('swiperight', handleSwipe);
    box.addEventListener('mousedown', handleTouch);
    box.addEventListener('touchstart', handleTouch);
    function handleSwipe(event) {
      switch (event.type) {
        case 'swipeleft':
          move('left');
          break;
    
        case 'swiperight':
          move('right');
          break;
        default:
      }
    }
  }
  
  function handleTouch (e) {
    var event = e;
    event = ('changedTouches' in event ? event.changedTouches[0] : event);
    switch (e.type) {
      case 'mousedown':
      case 'touchstart':
        touch.end = null;
        touch.move = null;
        touch.start = event.pageX;
        window.removeEventListener('mouseup', handleTouch);
        window.removeEventListener('touchend', handleTouch);
        window.removeEventListener('mousemove', handleTouch);
        window.removeEventListener('touchmove', handleTouch);
        window.addEventListener('mouseup', handleTouch);
        window.addEventListener('touchend', handleTouch);
        window.addEventListener('mousemove', handleTouch);
        window.addEventListener('touchmove', handleTouch);
        break;
      case 'mousemove':
      case 'touchmove':
        touch.move = event.pageX;
        break;
      case 'mouseup':
      case 'touchend':
        touch.end = touch.start - touch.move;
  
        if (touch.move == null || Math.abs(touch.end) < 32) {
          return;
        }
  
        if (touch.end > 0) {
          move('left');
        } else if (touch.end < 0) {
          move('right');
        }
  
        window.removeEventListener('mouseup', handleTouch);
        window.removeEventListener('touchend', handleTouch);
        window.removeEventListener('mousemove', handleTouch);
        window.removeEventListener('touchmove', handleTouch);
        break;
      default:
    }
  }
  
  function move (dir) {
    current = document.getElementsByClassName('current')[0];
    
    if (dir === 'left' && current.nextElementSibling === null || dir === 'right' && current.previousElementSibling === null) {
      return;
    }
  
    if (dir === 'left') {
      current.classList.add('toLeft');
      current.nextElementSibling.classList.add('current');
      current.classList.remove('current');
    } else {
      current.previousElementSibling.classList.add('current');
      current.previousElementSibling.classList.remove('toLeft');
      current.classList.remove('current');
    }
  
  }
});