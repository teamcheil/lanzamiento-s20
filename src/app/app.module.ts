import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { FormsModule } from '@angular/forms';
import {
  RECAPTCHA_SETTINGS,
  RecaptchaSettings,
  RecaptchaLoaderService,
  RecaptchaModule,
} from 'ng-recaptcha';
import { HttpClientModule } from '@angular/common/http';
import { BsModalService } from 'ngx-bootstrap'
import { ModalModule } from 'ngx-bootstrap';
import * as settings from "../assets/config.json";
const globalSettings: RecaptchaSettings = { siteKey: '6Lew4qkZAAAAAK50VC6HnGpNerA20BbixccSFcTh' };

import { SlickCarouselModule } from 'ngx-slick-carousel';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    SlickCarouselModule,
    ModalModule.forRoot(),
    RecaptchaModule.forRoot()
  ],
  providers: [
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: globalSettings,
    },
    HttpClientModule,
    BsModalService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }