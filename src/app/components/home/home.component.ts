import { Component, OnInit, ViewChild, TemplateRef,ElementRef, ViewChildren  } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { NgForm } from '@angular/forms';
import * as settings from "../../../assets/config.json";

declare global {
  interface Window {
      dataLayer:any;
  }
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})

export class HomeComponent {
  public email = "";
  public recaptchaStr = '';
  public bsModalRef: BsModalRef;
  public SITE_KEY_V3 = settings.recaptcha_key_v3;
  public videoPlaying = false;
  public submiting = false;
  public slides = [3,4,1,2];
  public slideConfig = {
    "slidesToShow": 3,
    "slidesToScroll": 1,
    "nextArrow": "<div class='nav-btn next-slide'></div>",
    "prevArrow": "<div class='nav-btn prev-slide'></div>",
    "dots": true,
    "infinite": false,
    responsive: [
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 1
        }
      },
    ]
  };
  public firstName: any;
  public lastName: any;
  public phone: any;
  public province: any;
  public favorite: any;
  public promotionalCode: any;
  public acceptedPrivacyPolicy: any;
  public acceptedTermsOfService: any;
  public marketingOptIn: any;
  public store: any;

  public YT: any;
  public video: any;
  public player: any;
  public reframed: Boolean = false;
  public showPause: boolean = true;

  @ViewChild("templateTerms", {static:false}) templateTerms: TemplateRef<any>;
  @ViewChild("templateSuccess", {static:false}) template: TemplateRef<any>;
  @ViewChild("templateRetailers1", {static:false}) template1: TemplateRef<any>;
  @ViewChild("templateRetailers2", {static:false}) template2: TemplateRef<any>;
  @ViewChild("templateRetailers3", {static:false}) template3: TemplateRef<any>;
  @ViewChild("templateRetailers4", {static:false}) template4: TemplateRef<any>;

  constructor(
    private http:HttpClient,
    private modalService: BsModalService) { }

  resolved2(captchaResponse: string): void {
      this.recaptchaStr = captchaResponse;
      if (this.recaptchaStr) {
          this.onLoginSubmit();
      }
  }

  openModal(){
    console.log("MODAL OPEN");
    this.bsModalRef = this.modalService.show(  
      this.template,  
      Object.assign({}, { class: 'gray modal-lg' })  
    );  
  }

  onLoginSubmit(){
    let element = document.getElementById("email-input-s20");
      element.classList.remove("error");
      let headers = new HttpHeaders().set('Content-Type', 'application/json');
      this.http.post("https://api.samsung.com.pe/global/subscribe", {"email": this.email, "recaptcha":  this.recaptchaStr}, {headers}).subscribe(data => {
          console.log(data);
          this.openModal();
          this.email = "";
          window.dataLayer.push({
                'event': 'ajaxComplete',
                'attributes': {
                'type': 'POST',
                'url': location.href,
                'pathname': location.pathname,
                'hostname': location.hostname || '',
                'protocol': location.protocol || '',
                'fragment': location.hash || '',
                'timestamp': Date.now(),
                'contentType': 'JSON',
                'response': data["message"] || ''
              }
            });
      });
  }
  init() {
    var tag = document.createElement('script');
    tag.src = 'https://www.youtube.com/iframe_api';
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  }
  
  playFullscreen(){
    this.player.playVideo();//won't work on mobile
    var point = document.querySelector.bind(document);
    let iframe = point('#player');
    var requestFullScreen = iframe.requestFullScreen || iframe.mozRequestFullScreen || iframe.webkitRequestFullScreen;
    if (requestFullScreen) {
      requestFullScreen.bind(iframe)();
    }
  }

  onPlayerStateChange(event) {
    console.log(event)
    switch (event.data) {
      case window['YT'].PlayerState.PLAYING:
        if (this.cleanTime() == 0) {
          console.log('started ' + this.cleanTime());
        } else {
          console.log('playing ' + this.cleanTime())
        };
        break;
      case window['YT'].PlayerState.PAUSED:
        if (this.player.getDuration() - this.player.getCurrentTime() != 0) {
          console.log('paused' + ' @ ' + this.cleanTime());
        };
        break;
      case window['YT'].PlayerState.ENDED:
        console.log('ended ');
        break;
    };
  };

  //utility
  cleanTime() {
    return Math.round(this.player.getCurrentTime())
  };

  onPlayerError(event) {
      switch (event.data) {
        case 2:
          console.log('' + this.video)
          break;
        case 100:
          break;
        case 101 || 150:
          break;
      };
  };

  resolved(captchaResponse: string, myForm: NgForm): void {
    this.recaptchaStr = captchaResponse;
    if (this.recaptchaStr) {
        this.saveSorteo(myForm);
    }else{
      this.submiting = false;
    }
  }

  saveSorteo(myForm:NgForm){
    this.submiting = true;
    let postData = {
      "campaingId": "digitalLaunch2020",
      "firstName": this.firstName,
      "lastName": this.lastName,
      "phone": this.phone,
      "email": this.email,
      "province": this.province,
      "custom01": this.favorite,
      "custom02": this.promotionalCode,
      "custom03": this.store,
      "acceptedPrivacyPolicy": this.acceptedPrivacyPolicy,
      "acceptedTermsOfService": this.acceptedTermsOfService,
      "marketingOptIn": this.marketingOptIn,
      "username": "sepr_cheil_qled_digital_launch_2020",
      "password": "B13s@546!@#Az_d",
      "testRecord": false
    } 

    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    this.http.post("https://www.samsung.com.pe/samsung-global-api/api/v1/users", postData, {headers}).subscribe(data => {
        console.log(data);
        window.dataLayer.push({
          'event': 'ajaxComplete',
          'attributes': {
          'type': 'POST',
          'url': location.href,
          'pathname': location.pathname,
          'hostname': location.hostname || '',
          'protocol': location.protocol || '',
          'fragment': location.hash || '',
          'timestamp': Date.now(),
          'contentType': 'JSON',
          'response': data["message"] || '',
          "data": 'Created'
        }
      });
      this.openModalSuccess();
      this.submiting = false;
      myForm.reset();
    });
  }

  clickFormContador(captchaRef: any) {
    console.log("form contador")
    if (this.recaptchaStr) {
        captchaRef.reset();
    }
    captchaRef.execute();
  }

  clickFormSorteo(captchaRef: any) {
    this.submiting = true;
    if (this.recaptchaStr) {
        captchaRef.reset();
    }
    captchaRef.execute();
  }

  openTerms(){
    this.bsModalRef = this.modalService.show(  
      this.templateTerms,  
      Object.assign({}, { class: 'gray modal-lg' })  
    );  
  }
  openModalSuccess(){
    this.bsModalRef = this.modalService.show(  
      this.template,  
      Object.assign({}, { class: 'gray modal-lg' })  
    );  
  }

  getDataHttp(url:string) {

      return this.http.get(url);
  }

  playVideo(){
    this.videoPlaying = true;
    this.init();
    var url = "/galaxy2020/assets/config.json?nocache="+ (new Date()).getTime();        
    this.video = "uertWoKtlXo";

    window['onYouTubeIframeAPIReady'] = (e) => {
      this.YT = window['YT'];
      this.player = new window['YT'].Player('player', {
        videoId: this.video,
        playerVars: {
          autoplay: 1,
          modestbranding: 1,
          controls: 1,
          disablekb: 1,
          rel: 0,
          showinfo: 0,
          fs: 0,
          playsinline: 1,
          origin:'http://localhost:4200'
        },
        events: {
          'onStateChange': this.onPlayerStateChange.bind(this),
          'onError': this.onPlayerError.bind(this),
          'onReady': (e) => {
            this.player.playVideo();
          }
        }
      });
    };
  }

  playYT(){
    this.player.playVideo();
    this.showPause = true;
  }

  pauseYT(){
    this.player.pauseVideo();
    this.showPause = false;
  }
  
  scroll(el: HTMLElement) {
    el.scrollIntoView({behavior:"smooth"});
  }

  openModalRetailer(slide){
    let template:any = 0;
    if(slide ==1){
      template = this.template1;
    }
    if(slide ==2){
      template = this.template2;
    }
    if(slide ==3){
      template = this.template3;
    }
    if(slide ==4){
      template = this.template4;
    }
    this.bsModalRef = this.modalService.show(  
      template,  
      Object.assign({}, { class: 'gray modal-lg' })  
    );  
  }

  slickInit(e) {
    console.log('slick initialized');
  }

  breakpoint(e) {
    console.log('breakpoint');
  }

  afterChange(e) {
    console.log('afterChange');
  }

  beforeChange(e) {
    console.log('beforeChange');
  }
}